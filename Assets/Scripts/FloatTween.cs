using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatTween : MonoBehaviour
{

    [SerializeField] Vector3 animScaleValue = new Vector3(1.05f, 1.05f, 1);
    [SerializeField] float animTime = 0.5f;

    [SerializeField] LeanTweenType animationType;
    // Start is called before the first frame update
    void Start()
    {
        LeanTween.scale(gameObject, animScaleValue, animTime).setLoopType(animationType).setIgnoreTimeScale(true);


        //LeanTween.scaleX(gameObject, horizontalAnimValue, horizontalAnimTime).setEaseInOutBack(). setLoopType(horizontalAnimationType);

        //LeanTween.scaleY(gameObject, verticalAnimValue, verticalAnimTime).setEaseInOutBack() .setLoopType(verticalAnimationType);


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
