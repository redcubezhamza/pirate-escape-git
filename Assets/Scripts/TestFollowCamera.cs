using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestFollowCamera : MonoBehaviour
{
    [SerializeField] float zOffset = 20;
    [SerializeField] float yOffset = 10;
    [SerializeField] GameObject player;
    // Start is called before the first frame update
    void Start()
    {
      //  player = GameObject.Find("PlayerBoat");
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y + yOffset, player.transform.position.z + zOffset);
        transform.LookAt(player.transform.position);
    }
}
