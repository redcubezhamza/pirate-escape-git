using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using TMPro;
using UnityEngine.UI;

public class PlayfabManager : MonoBehaviour
{
    public static PlayfabManager instance;
    void Awake()
    {
        PlayFabSettings.TitleId = playfabTitleID; //your title id goes here.
        instance = this;
    }
    [Header("Playfab Settings")]
    [SerializeField] string playfabTitleID;
    [SerializeField] string leaderboardStatisticName;

    [Header("Leaderboard Objects")]

    [SerializeField] bool isLeaderboardUpdated = false;

    [SerializeField] GameObject loginErrorWindowObject;

    public bool isPlayfabLoggedIn = false;



    public GameObject rowPrefab, row2Prefab, nameWindow, leaderboardHeader, leaderboardWindow;
    public Transform rowParent;



    public bool LeaderboardButtonPressed = false;
    public TextMeshProUGUI messageText, nameErrorText/*, loginErrorText*/;
    public TMP_InputField emailInput;
    public TMP_InputField passwordInput;
    public TMP_InputField nameInput;


    [Header("Startup Name Window Objects")]
    [SerializeField] GameObject startupNameWindow;
    [SerializeField] TextMeshProUGUI startupNameErrorText, startupWelcomeUserTextObject;
    [SerializeField] TMP_InputField startupNameInput;


    public void RegisterButton()
    {
        if (passwordInput.text.Length < 6)
        {
            messageText.text = "Email too short";
        }


        var request = new RegisterPlayFabUserRequest
        {
            Email = emailInput.text,
            Password = passwordInput.text,
            RequireBothUsernameAndEmail = false

        };

        PlayFabClientAPI.RegisterPlayFabUser(request, OnRegisterSuccess, OnError);
    }


    void OnRegisterSuccess(RegisterPlayFabUserResult result)
    {
        messageText.text = "Registered & Logged in Successfully";
    }


    public void LoginButton()
    {
        var request = new LoginWithEmailAddressRequest
        {
            Email = emailInput.text,
            Password = passwordInput.text

        };
        PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnError);

    }

    void OnLoginSuccess(LoginResult result)
    {
        messageText.text = "Logged in Success";
        Debug.Log("Successful Login");


    }



    public void ResetPasswordButton()
    {
        var request = new SendAccountRecoveryEmailRequest
        {
            Email = emailInput.text,
            TitleId = playfabTitleID

        };

        PlayFabClientAPI.SendAccountRecoveryEmail(request, OnPasswordReset, OnError);
    }


    void OnPasswordReset(SendAccountRecoveryEmailResult result)
    {
        messageText.text = "Password reset mail sent";
    }







    //new code























    //public TextMeshProUGUI gemsValueText;



    // Start is called before the first frame update
    void Start()
    {


    }




    // Update is called once per frame



    public void Login()
    {
        var request = new LoginWithCustomIDRequest
        {
            CustomId = SystemInfo.deviceUniqueIdentifier,


            // CustomId = Random.Range(1000, 300000).ToString(),

            CreateAccount = true,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
            {
                GetPlayerProfile = true
            }

        };

        PlayFabClientAPI.LoginWithCustomID(request, OnSuccess, OnLeaderboardLoginError);
    }

    void OnSuccess(LoginResult result)
    {
        Debug.Log("Login Successfull");

        isPlayfabLoggedIn = true;
        PlayerPrefs.SetInt("loggedin", 1);

        //   loginErrorText.gameObject.SetActive(false);

        loginErrorWindowObject.gameObject.SetActive(false);

        //SendLeaderboard(Random.Range(0, 100));

        // SendLeaderboard(PlayerPrefs.GetInt("PlayerScore"));

        SendLeaderboard(PlayerPrefs.GetInt("bestscore", 0));           //sending the highest scores stored locally to leaderboard

        GetVirtualCurrencies();
        string username = null;



        if (result.InfoResultPayload.PlayerProfile != null)
        {
            username = result.InfoResultPayload.PlayerProfile.DisplayName;
            PlayerPrefs.SetString("username", username);
        }


        if (username == null)

            nameWindow.SetActive(true);

        else
        {
            leaderboardWindow.SetActive(true);
            GetLeaderboard();
            //   StartCoroutine(GetLeaderboardWithDelay(1f));
        }

        //StartCoroutine(GetLeaderboardWithDelay(1f));
        //else

        //    leaderboardWindow.SetActive(true);
        //GetLeaderboard();

    }

    void OnLeaderboardLoginError(PlayFabError error)
    {
        isPlayfabLoggedIn = false;
        PlayerPrefs.SetInt("loggedin", 0);


        Debug.Log("Error while logging In/Creating account");
        Debug.Log(error.GenerateErrorReport());

        //    loginErrorText.gameObject.SetActive(true);

        loginErrorWindowObject.gameObject.SetActive(true);


        //loginErrorText.text = "Leaderboard Login Error:" + error.GenerateErrorReport();

        //    loginErrorText.text = "Leaderboard error, please check your internet connection";



        //    StartCoroutine(errorTextDisable(loginErrorText.gameObject));

        StartCoroutine(errorTextDisable(loginErrorWindowObject));
    }

    public void GetVirtualCurrencies()
    {
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), OnGetUserInventorySuccess, OnError);
    }


    void OnGetUserInventorySuccess(GetUserInventoryResult result)
    {
        //int gems = result.VirtualCurrency["GM"];
        //gemsValueText.text = gems.ToString();
    }


    public void SubmitNameButton()
    {
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = nameInput.text,

            //DisplayName = PlayerPrefs.GetString("username")


        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnDisplayNameUpdate, OnDisplayNameError);
    }



    void OnDisplayNameError(PlayFabError error)
    {
        nameErrorText.text = "Name Error:" + error.GenerateErrorReport() /*+ "| Make sure your name should be unique & without special characters"*/;

        StartCoroutine(errorTextDisable(nameErrorText.gameObject));
    }

    void OnDisplayNameUpdate(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("Updated Display Name");

        PlayerPrefs.SetString("username", nameInput.text);

        nameWindow.SetActive(false);
        leaderboardWindow.SetActive(true);
        GetLeaderboard();

    }

    void OnError(PlayFabError error)
    {
        Debug.Log("Error while logging In/Creating account");
        Debug.Log(error.GenerateErrorReport());
    }


    public void SendLeaderboard(int score)
    {
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = leaderboardStatisticName,
                    Value = score,


                }

            }
        };

        isLeaderboardUpdated = true;

        Debug.Log("score sent to leaderboard " + PlayerPrefs.GetInt("PlayerScore") + " and isLeaderboardactive bool is" + isLeaderboardUpdated.ToString());
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderboardUpdate, OnSendLeaderboardError);

    }

    void OnSendLeaderboardError(PlayFabError error)
    {
        Debug.Log("Unable to send score to Leaderboard" + error);
        PlayerPrefs.SetInt("loggedin", 0);
    }



    void OnLeaderboardUpdate(UpdatePlayerStatisticsResult result)
    {
        Debug.Log("Leaderboard Updated Successfully");

        PlayerPrefs.SetInt("loggedin", 1);
    }

    public void GetLeaderboard()
    {
        var request = new GetLeaderboardRequest
        {
            StatisticName = leaderboardStatisticName,
            StartPosition = 0,
            MaxResultsCount = 10
        };

        PlayFabClientAPI.GetLeaderboard(request, OnLeaderboardGet, OnError);

    }


    void OnLeaderboardGet(GetLeaderboardResult result)
    {

        int a = 0;



        foreach (Transform item in rowParent)
        {
            Destroy(item.gameObject);
        }

        GameObject headerGO = Instantiate(leaderboardHeader, rowParent);

        foreach (var item in result.Leaderboard)
        {
            a++;

            if (a % 2 != 0)
            {
                GameObject newGO = Instantiate(row2Prefab, rowParent);
                TextMeshProUGUI[] texts = newGO.GetComponentsInChildren<TextMeshProUGUI>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                //Debug.Log(item.Position + " " + item.PlayFabId + " " + item.StatValue);
                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }
            else
            {
                GameObject newGO = Instantiate(rowPrefab, rowParent);                                      //black & white rows for even and odd enteries in leaderboard
                TextMeshProUGUI[] texts = newGO.GetComponentsInChildren<TextMeshProUGUI>();

                texts[0].text = (item.Position + 1).ToString();
                texts[1].text = item.DisplayName;
                texts[2].text = item.StatValue.ToString();

                //Debug.Log(item.Position + " " + item.PlayFabId + " " + item.StatValue);
                Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
            }



        }
    }


    IEnumerator errorTextDisable(GameObject errorTextObject)
    {
        yield return new WaitForSecondsRealtime(5);
        errorTextObject.gameObject.SetActive(false);
        Debug.Log("login error text disabled");
    }


    IEnumerator GetLeaderboardWithDelay(float delayTime)           //coroutine to give little delay while leaderboard updates with current score
    {
        yield return new WaitForSeconds(delayTime);
        // yield return new WaitUntil(() => isLeaderboardUpdated == true);
        GetLeaderboard();
    }



    // Exclusive code for registering name on startup of game....................................................................



    public void LaunchSubmitNameOnStart()
    {
        LoginOnly();
    }

    void LoginOnly()
    {
        var request = new LoginWithCustomIDRequest
        {
            CustomId = SystemInfo.deviceUniqueIdentifier,


            //  CustomId = Random.Range(1000, 300000).ToString(),

            CreateAccount = true,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
            {
                GetPlayerProfile = true
            }

        };

        PlayFabClientAPI.LoginWithCustomID(request, OnStartupLoginSuccess, OnStartupLoginError);
    }

    void OnStartupLoginSuccess(LoginResult result)
    {
        isPlayfabLoggedIn = true;
        PlayerPrefs.SetInt("loggedin", 1);


        Debug.Log("Startup Login Successfull");

        //  loginErrorText.gameObject.SetActive(false);
        //SendLeaderboard(Random.Range(0, 100));

        //  SendLeaderboard(PlayerPrefs.GetInt("PlayerScore"));
        //  GetVirtualCurrencies();
        string username = null;

        if (result.InfoResultPayload.PlayerProfile != null)
        {
            username = result.InfoResultPayload.PlayerProfile.DisplayName;
            PlayerPrefs.SetString("username", username);
            //   startupWelcomeUserTextObject.text = "Welcome " + PlayerPrefs.GetString("username");
        }

        if (username == null)
            startupNameWindow.SetActive(true);
        //else
        //   leaderboardWindow.SetActive(true);
        // GetLeaderboard();
    }
    void OnStartupLoginError(PlayFabError error)
    {
        Debug.Log("Error while logging In/Creating account");
        Debug.Log(error.GenerateErrorReport());

        isPlayfabLoggedIn = false;
        PlayerPrefs.SetInt("loggedin", 0);            //to send leaderboard scores from game manager only if its playfab is logged in
    }


    public void StartupSubmitNameButton()
    {
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = startupNameInput.text,

            //DisplayName = PlayerPrefs.GetString("username")


        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnStartupNameUpdateSuccess, OnStartupNameError);
    }



    void OnStartupNameError(PlayFabError error)
    {
        startupNameErrorText.text = "Name Error:" + error.GenerateErrorReport() /*+ "| Make sure your name should be unique & without special characters"*/;

        StartCoroutine(errorTextDisable(startupNameErrorText.gameObject));


    }

    void OnStartupNameUpdateSuccess(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("Startup Name submit success");

        PlayerPrefs.SetString("username", startupNameInput.text);

        startupWelcomeUserTextObject.text = "Welcome " + PlayerPrefs.GetString("username");

        startupNameWindow.gameObject.SetActive(false);
    }




}




#region Old Playfab Code

//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using TMPro;
//using PlayFab;
//using PlayFab.ClientModels;
//using UnityEngine.UI;
//using System;

//public class PlayfabManager : MonoBehaviour
//{
//    public static PlayfabManager instance;



//    public GameObject rowPrefab, nameWindow, leaderboardWindow;
//    public Transform rowParent;


//    public TextMeshProUGUI messageText;
//    public TMP_InputField emailInput;
//    public TMP_InputField passwordInput;
//    public TMP_InputField nameInput;

//    public TextMeshProUGUI gemsValueText;
//    public TextMeshProUGUI energyValueText;
//    public TextMeshProUGUI energyRechargeTimeText;

//    [Header("Variables")]
//    [SerializeField] float secondsLeftToRefreshEnergy = 1;
//    public bool LeaderboardButtonPressed = false;


//    void Awake()
//    {
//        PlayFabSettings.TitleId = "92CD3"; //your title id goes here.
//        instance = this;
//    }

//    public void RegisterButton()
//    {
//        if (passwordInput.text.Length < 6)
//        {
//            messageText.text = "Email too short";
//        }


//        var request = new RegisterPlayFabUserRequest
//        {
//            Email = emailInput.text,
//            Password = passwordInput.text,
//            RequireBothUsernameAndEmail = false

//        };

//        PlayFabClientAPI.RegisterPlayFabUser(request, OnRegisterSuccess, OnError);
//    }


//    void OnRegisterSuccess(RegisterPlayFabUserResult result)
//    {
//        messageText.text = "Registered & Logged in Successfully";
//    }


//    public void LoginButton()
//    {
//        var request = new LoginWithEmailAddressRequest
//        {
//            Email = emailInput.text,
//            Password = passwordInput.text

//        };
//        PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnError);

//    }

//    void OnLoginSuccess(LoginResult result)
//    {
//        messageText.text = "Logged in Success";
//        Debug.Log("Successful Login");




//    }


//    public void ResetPasswordButton()
//    {
//        var request = new SendAccountRecoveryEmailRequest
//        {
//            Email = emailInput.text,
//            TitleId = "2B0E0"

//        };

//        PlayFabClientAPI.SendAccountRecoveryEmail(request, OnPasswordReset, OnError);
//    }


//    void OnPasswordReset(SendAccountRecoveryEmailResult result)
//    {
//        messageText.text = "Password reset mail sent";
//    }





//    //new code























//    //public TextMeshProUGUI gemsValueText;



//    // Start is called before the first frame update
//    void Start()
//    {

//        Login();

//    }




//    // Update is called once per frame
//    private void Update()
//    {
//        secondsLeftToRefreshEnergy -= Time.deltaTime;
//        TimeSpan time = TimeSpan.FromSeconds(secondsLeftToRefreshEnergy);
//        energyRechargeTimeText.text = time.ToString("mm':'ss");
//        if (secondsLeftToRefreshEnergy < 0)
//        {
//            GetVirtualCurrencies();
//        }
//    }



//    public void Login()
//    {
//        var request = new LoginWithCustomIDRequest
//        {
//            CustomId = SystemInfo.deviceUniqueIdentifier,
//            CreateAccount = true,
//            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
//            {
//                GetPlayerProfile = true
//            }

//        };

//        PlayFabClientAPI.LoginWithCustomID(request, OnSuccess, OnError);
//    }

//    void OnSuccess(LoginResult result)
//    {
//        Debug.Log("Login Successfull");

//        SendLeaderboard(UnityEngine.Random.Range(0, 100));

//        //SendLeaderboard(PlayerPrefs.GetInt("PlayerScore"));
//        GetVirtualCurrencies();
//        //string username = null;

//        //if (result.InfoResultPayload.PlayerProfile != null)
//        //    username = result.InfoResultPayload.PlayerProfile.DisplayName;

//        //if (username == null)

//        //    nameWindow.SetActive(true);

//        //else

//        //    leaderboardWindow.SetActive(true);

//    }

//    public void GetVirtualCurrencies()
//    {
//        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), OnGetUserInventorySuccess, OnError);
//    }


//    void OnGetUserInventorySuccess(GetUserInventoryResult result)
//    {
//        int Gems = result.VirtualCurrency["GM"];
//        gemsValueText.text = Gems.ToString();

//        int Energy = result.VirtualCurrency["EN"];
//        energyValueText.text = Energy.ToString();
//        secondsLeftToRefreshEnergy = result.VirtualCurrencyRechargeTimes["EN"].SecondsToRecharge;

//    }


//    public void SubmitNameButton()
//    {
//        var request = new UpdateUserTitleDisplayNameRequest
//        {
//            DisplayName = nameInput.text,
//        };
//        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnDisplayNameUpdate, OnError);
//    }

//    void OnDisplayNameUpdate(UpdateUserTitleDisplayNameResult result)
//    {
//        Debug.Log("Updated Display Name");
//        nameWindow.SetActive(false);
//        leaderboardWindow.SetActive(true);

//    }

//    void OnError(PlayFabError error)
//    {
//        Debug.Log("Error while logging In/Creating account");
//        Debug.Log(error.GenerateErrorReport());
//    }


//    public void SendLeaderboard(int score)
//    {
//        var request = new UpdatePlayerStatisticsRequest
//        {
//            Statistics = new List<StatisticUpdate>
//            {
//                new StatisticUpdate
//                {
//                    StatisticName = "Score Leaderboard",
//                    Value = score
//                }

//            }
//        };
//        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderboardUpdate, OnError);

//    }

//    void OnLeaderboardUpdate(UpdatePlayerStatisticsResult result)
//    {
//        Debug.Log("Leaderboard Updated Successfully");
//    }

//    public void GetLeaderboard()
//    {
//        var request = new GetLeaderboardRequest
//        {
//            StatisticName = "Score Leaderboard",
//            StartPosition = 0,
//            MaxResultsCount = 10
//        };

//        PlayFabClientAPI.GetLeaderboard(request, OnLeaderboardGet, OnError);

//    }


//    void OnLeaderboardGet(GetLeaderboardResult result)
//    {
//        foreach (Transform item in rowParent)
//        {
//            Destroy(item.gameObject);
//        }
//        foreach (var item in result.Leaderboard)
//        {
//            GameObject newGO = Instantiate(rowPrefab, rowParent);
//            Text[] texts = newGO.GetComponentsInChildren<Text>();

//            texts[0].text = (item.Position + 1).ToString();
//            texts[1].text = item.DisplayName;
//            texts[2].text = item.StatValue.ToString();

//            //Debug.Log(item.Position + " " + item.PlayFabId + " " + item.StatValue);
//            Debug.Log(string.Format("Position: {0} | Name: {1} | Score: {2}", item.Position, item.PlayFabId, item.StatValue));
//        }
//    }
//}
#endregion
