using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Gui;
using TMPro;


public class CoinsManager : MonoBehaviour
{
    [SerializeField] GameObject coinPrefab;
    [SerializeField] Collider walkablePath;


    private void Start()
    {
        InvokeRepeating("SpawnCoins", 5, 25);
    }


    void SpawnCoins()
    {
        int coinsToSpawn = 5;
        for (int i = 0; i < coinsToSpawn; i++)
        {
            GameObject temp = Instantiate(coinPrefab, transform);
            temp.transform.position = GetRandomPointInCollider(walkablePath);
        }
    }

    Vector3 GetRandomPointInCollider(Collider collider)
    {
        Vector3 point = new Vector3(
            Random.Range(collider.bounds.min.x + 5, collider.bounds.max.x -5),
            Random.Range(collider.bounds.min.y, collider.bounds.max.y),
            Random.Range(collider.bounds.min.z, collider.bounds.max.z)
            );
        if (point != collider.ClosestPoint(point))
        {
            point = GetRandomPointInCollider(collider);
        }

        point.y = 1.5f;
        return point;
    }







    ////References
    //[Header("UI references")]
    //[SerializeField] TextMeshProUGUI coinUIText;
    //[SerializeField] GameObject animatedCoinPrefab;
    //[SerializeField] Transform target;

    //[Space]
    //[Header("Available coins : (coins to pool)")]
    //[SerializeField] int maxCoins;
    //Queue<GameObject> coinsQueue = new Queue<GameObject>();


    //[Space]
    //[Header("Animation settings")]
    //[SerializeField][Range(0.5f, 0.9f)] float minAnimDuration;
    //[SerializeField][Range(0.9f, 2f)] float maxAnimDuration;

    //[SerializeField] LeanTweenType easeType;
    //[SerializeField] float spread;

    //Vector3 targetPosition;


    //private int _c = 0;

    //public int Coins
    //{
    //    get { return _c; }
    //    set
    //    {
    //        _c = value;
    //        //update UI text whenever "Coins" variable is changed
    //        coinUIText.text = Coins.ToString();
    //    }
    //}

    //void Awake()
    //{
    //    targetPosition = target.position;

    //    //prepare pool
    //    PrepareCoins();
    //}

    //void PrepareCoins()
    //{
    //    GameObject coin;
    //    for (int i = 0; i < maxCoins; i++)
    //    {
    //        coin = Instantiate(animatedCoinPrefab);
    //        coin.transform.parent = transform;
    //        coin.SetActive(false);
    //        coinsQueue.Enqueue(coin);
    //    }
    //}

    //void Animate(Vector3 collectedCoinPosition, int amount)
    //{
    //    for (int i = 0; i < amount; i++)
    //    {
    //        //check if there's coins in the pool
    //        if (coinsQueue.Count > 0)
    //        {
    //            //extract a coin from the pool
    //            GameObject coin = coinsQueue.Dequeue();
    //            coin.SetActive(true);

    //            //move coin to the collected coin pos
    //            coin.transform.position = collectedCoinPosition + new Vector3(Random.Range(-spread, spread), 0f, 0f);

    //            //animate coin to target position
    //            float duration = Random.Range(minAnimDuration, maxAnimDuration);
    //            coin.transform.LeanMove (targetPosition, duration)
    //            .setEase(easeType)
    //            .setOnComplete(() => {
    //                //executes whenever coin reach target position
    //                coin.SetActive(false);
    //                coinsQueue.Enqueue(coin);

    //                Coins++;
    //            });
    //        }
    //    }
    //}

    //public void AddCoins(Vector3 collectedCoinPosition, int amount)
    //{
    //    Animate(collectedCoinPosition, amount);
    //}
}
