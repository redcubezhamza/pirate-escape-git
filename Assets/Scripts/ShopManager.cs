using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class ShopManager : MonoBehaviour
{
    private int Gems;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void On200GemsPurchaseComplete(Product product)
    {
       
        int gem = PlayerPrefs.GetInt("playergems");
        gem += 200;
        PlayerPrefs.SetInt("playergems", gem);
        GameObject.Find("_scripts").GetComponent<MainMenuManager>().UpdateGems();          //updating gems on miain menu
        Debug.Log("Purchased Gems: " + PlayerPrefs.GetInt("playergems"));
    }

    public void On500GemsPurchaseComplete(Product product)
    {
        
        int gem = PlayerPrefs.GetInt("playergems");
        gem += 500;
        PlayerPrefs.SetInt("playergems", gem);


        //if (currentSceneName == "MainMenu")
        //{
           GameObject.Find("_scripts").GetComponent<MainMenuManager>().UpdateGems();
        //}
        Debug.Log("Purchased Gems: " + PlayerPrefs.GetInt("playergems"));
    }

    public void On1000GemsPurchaseComplete(Product product)
    {
        int gem = PlayerPrefs.GetInt("playergems");
        gem += 1000;
        PlayerPrefs.SetInt("playergems", gem);


        //if (currentSceneName == "MainMenu")
        //{
            GameObject.Find("_scripts").GetComponent<MainMenuManager>().UpdateGems();
        //}
        Debug.Log("Purchased Gems: " + PlayerPrefs.GetInt("playergems"));
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log("Purchase failed due to " + failureReason.ToString());
    }


    public void AdButton()
    {
        RewardedAdmob.Instance.ShowRewarded();

    }


   
}
