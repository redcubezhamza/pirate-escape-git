using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeBar : MonoBehaviour
{


    public Slider slider;
    public Gradient gradient;
    public Image fill;
    // Start is called before the first frame update
    public void SetMaxHealth(int health)
    {
        fill.color = gradient.Evaluate(1f);
        slider.value = health;
        slider.maxValue = health;
    }

    public void SetValue(int health)
    {
        slider.value -= health;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }

    private void Update()
    {
        if (gameObject.name == "TimeBar")
        {
            slider.value -= Time.deltaTime;

            GameManager.instance.timeLeftText.text = slider.value.ToString("F0");
            

            fill.color = gradient.Evaluate(slider.normalizedValue);
        }


        if (gameObject.name == "BoatHealth")
        {



            slider.value += Time.deltaTime * 0.4f;
            fill.color = gradient.Evaluate(slider.normalizedValue);
        }
        
        if (slider.value <= 0)
        {
            GameManager.instance.GameOver();
        }
    }
}
