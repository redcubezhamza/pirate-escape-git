using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Gui;

public class PauseMenuManager : MonoBehaviour
{
    [SerializeField] float pauseTweenTime = 2f, resumeTweenTime = 0.5f;

    public bool isTouchEnabled = true;
    [SerializeField] GameObject touchControls;


    [Header("Pause Panel GameObjects")]
    [SerializeField] GameObject MainPausePanel;
    // Start is called before the first frame update
    void Start()
    {
        if (isTouchEnabled)
        {
            touchControls.gameObject.SetActive(true);
        }
        else
            touchControls.gameObject.SetActive(false);
        
    }

    
    void PauseTween()
    {
       LeanTween.cancel(gameObject);

        transform.localScale = Vector3.one * 2;

        LeanTween.scale(gameObject, Vector3.one * 1, pauseTweenTime).setEaseInBack() .setIgnoreTimeScale(true);

        
    }


    void ResumeTween()
    {
        LeanTween.scale(gameObject, Vector3.one * 0.5f, resumeTweenTime).setEaseOutExpo().setIgnoreTimeScale(true).setOnComplete(AfterResumeGameAnimation);
    }




    public void PauseGame()
    {
        Time.timeScale = 0;
        PauseTween();
        
    }


    public void ResumeGame()
    {
        //if (isTouchEnabled)
        //{
        //    touchControls.gameObject.SetActive(true);
        //}
        //else
        //    touchControls.gameObject.SetActive(false);

        ResumeTween();
        
    }

    void AfterResumeGameAnimation()
    {
        MainPausePanel.gameObject.SetActive(false);
        Time.timeScale = 1f;
    }
}
