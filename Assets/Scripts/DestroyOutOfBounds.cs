using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOutOfBounds : MonoBehaviour
{

    private GameObject playerBoat;
    [SerializeField] float destroyDistance = 40;
    // Start is called before the first frame update
    void Start()
    {
        playerBoat = GameObject.Find("PlayerBoat");
    }

    // Update is called once per frame
    void Update()
    {
        if (playerBoat.transform.position.z - transform.position.z > destroyDistance)
        {
            Destroy(gameObject);
            Debug.Log("Destroyed out of bounds" + gameObject.name);
        }
    }
}
