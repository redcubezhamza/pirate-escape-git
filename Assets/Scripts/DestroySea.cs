using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySea : MonoBehaviour
{
    [SerializeField] float destroyAfterDistance = 10;
    [SerializeField] bool destroyEnable;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(Camera.main.transform.position, gameObject.transform.position) > destroyAfterDistance && destroyEnable )
        {
            
            Destroy(gameObject);
            Debug.Log("Sea Prefab Destroyed");
        }
    }

    
}
