using Lean.Transition;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlyTweening : MonoBehaviour
{
    //  [SerializeField] GameObject tweenObject;
    [SerializeField] float tweenTime = 1;
    [SerializeField] LeanTweenType easeType = LeanTweenType.easeInOutExpo;
     

    [SerializeField] float initRepeatTime = 5, repeatAfterTime = 15;


    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Tween", initRepeatTime, repeatAfterTime);
        
    }

    public void Tween()
    {
        LeanTween.cancel(gameObject);
        gameObject.transform.localScale = Vector3.zero;

        LeanTween.scale(gameObject, Vector3.one, tweenTime)
            .setEase(easeType)
            .setDelay(tweenTime / 2);
    }
}
