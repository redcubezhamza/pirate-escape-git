using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [Header("Sea Elements")]
    [SerializeField] List<GameObject> seaPieces;
    [SerializeField] float seaSpawnOffset;

    [Space(10)]
    [Header("Obstacles Elements")]
    [SerializeField] int spawnInterval = 11;
    [SerializeField] int lastSpawnZ = 22;
    [SerializeField] int spawnAmount = 4;
    [SerializeField] float xPos;
    [SerializeField] List<GameObject> obstacles;

    [Space(10)]
    [Header("Coin Elements")]
    [SerializeField] int coinSpawnInterval;
    [SerializeField] int coinLastSpawnZ = 22;
    [SerializeField] int coinSpawnAmount = 4;
    [SerializeField] float coinXPos;
    [SerializeField] List<GameObject> coinPrefabs;


    public void MoveSea()
    {
        GameObject movedSea = seaPieces[0];
        seaPieces.Remove(movedSea);
        float newZ = seaPieces[seaPieces.Count - 1].transform.position.z + seaSpawnOffset;
        movedSea.transform.position = new Vector3(0, 0, newZ);
        seaPieces.Add(movedSea);

        Debug.Log("Next Sea Spawned");
    }



    public void SpawnObstacles()
    {
        lastSpawnZ += spawnInterval;

        int x = 15;
        for (int i = 0; i < spawnAmount; i++)
        {
            
            GameObject obstacle = obstacles[Random.Range(0, obstacles.Count)];
            //xPos = Random.Range(-40f, 40f);
            xPos = Random.Range(0, x);
            x *= -1;
            //lastSpawnZ += 50;
            Instantiate(obstacle, new Vector3(xPos, 1f, lastSpawnZ), obstacle.transform.rotation);
        }
    }



    public void SpawnCoins()
    {
        coinLastSpawnZ += coinSpawnInterval;

       //   for (int i = 0; i < coinSpawnAmount; i++)
      //   {

        GameObject coin = coinPrefabs[Random.Range(0, coinPrefabs.Count)];
        coinXPos = Random.Range(-15f, 15f);
        Instantiate(coin, new Vector3(xPos, 0, coinLastSpawnZ), coin.transform.rotation);
        Debug.Log("Coin SPawned");
       //  }


    }


    // old code
    //[SerializeField] private const float distanceToSpawnNextLevel = 300f;

    //[SerializeField] private GameObject playerCar;
    //[SerializeField] private Transform levelPartStart;

    //[SerializeField] private List<Transform> levelPartList;
    //private Vector3 lastEndPos;
    //private float previousLevelPosX;
    //// Start is called before the first frame update
    //void Start()
    //{

    //    //if (PlayerPrefs.GetInt("currentCar") == 0 || !PlayerPrefs.HasKey("currentCar"))
    //    //{
    //    //    playerCar = GameObject.Find("OldCar");
    //    //}

    //    //else if (PlayerPrefs.GetInt("currentCar") == 1)
    //    //{
    //    //    playerCar = GameObject.Find("RallyCar");
    //    //}

    //    //else if (PlayerPrefs.GetInt("currentCar") == 2)
    //    //{
    //    //    playerCar = GameObject.Find("PoliceCar");
    //    //}





    //    lastEndPos = levelPartStart.Find("EndPos").position;






    //}

    //// Update is called once per frame
    //void FixedUpdate()
    //{
    //    if (Vector3.Distance(playerCar.transform.position, lastEndPos) < distanceToSpawnNextLevel)
    //    {
    //        SpawnLevelPart();
    //    }
    //}

    //void SpawnLevelPart()
    //{
    //    Transform chosenLevelPart = levelPartList[Random.Range(0, levelPartList.Count)];
    //    Transform lastLevelTransform = SpawnLevel(chosenLevelPart, lastEndPos);      //sending current level end position to spawnLevel function to spawn next level there and getting next level transform in return after its instantiation
    //    lastLevelTransform = SpawnLevel(chosenLevelPart, lastLevelTransform.Find("EndPos").position);   //getting current level end position and again instantiating next level at current level end point

    //    lastEndPos = lastLevelTransform.Find("EndPos").position;  // storing ext level end point value to spawn next levels when distance limit reaches
    //}
    //Transform SpawnLevel(Transform levelPart, Vector3 spawnPosition)
    //{

    //    Transform currentLevelTransform = Instantiate(levelPart, spawnPosition, Quaternion.identity);
    //    return currentLevelTransform;
    //}
}
