using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StartupManager : MonoBehaviour
{
    [Tooltip("This Script is initializing ads & update name for new players")]
    [Header("Startup Script")]




    [SerializeField] TextMeshProUGUI userNameText;
    // Start is called before the first frame update
    void Start()
    {
        UpdateUsername();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void UpdateUsername()
    {
        if (!PlayerPrefs.HasKey("username"))
        {
            PlayerPrefs.SetString("username", "Guest");
            userNameText.text = "Welcome " + PlayerPrefs.GetString("username");
        }
        else
            userNameText.text = "Welcome " + PlayerPrefs.GetString("username");
    }
}
