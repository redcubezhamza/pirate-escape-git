using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private TimeBar timeBar, boatHealthBar;
   

    public bool isGameOver;

    [SerializeField] bool timeTrialMode, endlessMode, raceMode;
    public int timeTrialSeconds = 60, boatHealth;
    [SerializeField] GameObject gameOverPanel, gameOverSimplePanel;
    [SerializeField] GameObject[] playerBoats;
    public TextMeshProUGUI timeLeftText;
    [SerializeField] float distance;
    private GameObject playerBoat;
    public int gems;

    [SerializeField] AudioClip bGWaterSound;
    

    [Header("Text Objects")]
    [SerializeField] TextMeshProUGUI distanceText;

    // Start is called before the first frame update
    void Start()
    {
        // start Musics using Music Manager Script
        MusicManager.instance.PlayBGMusic(bGWaterSound);
        gems = PlayerPrefs.GetInt("playergems");



        // Screen.orientation = ScreenOrientation.LandscapeLeft;

        instance = this;
        GetGameMode();
        GetBoatProfile();
        Time.timeScale = 1;

        playerBoat = GameObject.Find("PlayerBoat");

        //BannerAdmob.Instance.RequestBanner();
        BannerAdmob.Instance.DestroyBanner();
        



    }

    // Update is called once per frame
    void Update()
    {
        //distance = Mathf.RoundToInt(playerBoat.transform.position.z);
        distance = playerBoat.transform.position.z;

        if (distance > 1000)
        {
            distance /= 1000;
            distanceText.text = distance.ToString("F2") + " Km";
        }
        else
        {
            distanceText.text = distance.ToString("F0") + " m";
        }
        

  
    }


    public void TryAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Exit()
    {
        SceneManager.LoadScene("MainMenu");
    }



    public void GameOver()
    {
        isGameOver = true;

        StartCoroutine(GameOverCoroutine());
    }



    IEnumerator GameOverCoroutine()
    {
        gameOverSimplePanel.gameObject.SetActive(true);
        yield return new WaitForSeconds(4);
        gameOverSimplePanel.gameObject.SetActive(false);
        gameOverPanel.gameObject.SetActive(true);
        Time.timeScale = 0;
    }
    void GetGameMode()
    {
        if (!PlayerPrefs.HasKey("gamemode"))
        {
            PlayerPrefs.SetInt("gamemode", 0);
        }
        else
            PlayerPrefs.GetInt("gamemode");

        if (PlayerPrefs.GetInt("gamemode") == 0)            //Time Trial Mode
        {
          //  timeBar = GameObject.Find("TimeBar").GetComponent<TimeBar>();                                        //time bar disabled

           // timeBar.SetMaxHealth(timeTrialSeconds);

            timeTrialMode = true;
        }
    }

    void GetBoatProfile()
    {
        if (!PlayerPrefs.HasKey("boatnumber"))
        {
            PlayerPrefs.SetInt("boatnumber", 0);
        }
        else
            PlayerPrefs.GetInt("boatnumber");

        if (PlayerPrefs.GetInt("boatnumber") == 0)            //Time Trial Mode
        {
            boatHealth = 100;

            playerBoats[0].gameObject.SetActive(true);

            boatHealthBar = GameObject.Find("BoatHealth").GetComponent<TimeBar>();

            boatHealthBar.SetMaxHealth(boatHealth);

        }
    }



    public void GemsScore(int gemsToAdd)
    {

        gems += gemsToAdd;
        PlayerPrefs.SetInt("playergems", gems);

    }
}
public class HUDManager : MonoBehaviour
{
    public static HUDManager instance;

    [Header("Text Objects")]
    [SerializeField] TextMeshProUGUI coinText;
    [SerializeField] TextMeshProUGUI gemsText;

    

    [Header("Scores")]
    public int score = 0;
    public int gems = 0;
    public float timeLeft = 60;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        



    }

    // Update is called once per frame
    void Update()
    {


        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    UpdateCoinScore(5);
        //}

       

    }



    //public void UpdateCoinScore(int coinScoreToAdd)
    //{

    //    score += coinScoreToAdd;
    //    coinText.text = score.ToString();
    //}

    public void UpdateCoinScore(int value)
    {
        score += value;

        // convert 1000 in K's
        if (score > 1000)
        {
            score /= 1000;
            coinText.text = score.ToString() + "K";
        }
        else
            coinText.text = score.ToString();

        // if scores are highest, then update Leaderboard
        //if (score > PlayerPrefs.GetInt("bestscore") && PlayerPrefs.GetInt("loggedin") == 1)
        //{
        //    PlayfabManager.instance.Login();
        //}

        

        // 1
        LeanTween.cancel(coinText.gameObject);
        coinText.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        coinText.transform.localScale = Vector3.one;

        // 2
        LeanTween.rotateZ(coinText.gameObject, 15.0f, 0.5f).setEasePunch();
        LeanTween.scaleX(coinText.gameObject, 1.5f, 0.5f).setEasePunch();
    }

    












}
