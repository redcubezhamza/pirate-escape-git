using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverManager : MonoBehaviour
{
    public TextMeshProUGUI scoreText;

    [SerializeField] float startTweenTime = 1f;

    // Start is called before the first frame update
    void Start()
    {

        transform.localScale = Vector3.one * 2;

        LeanTween.scale(gameObject, Vector3.one * 1, startTweenTime).setEaseInBack().setIgnoreTimeScale(true);


        scoreText.text = HUDManager.instance.score.ToString();

        
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
