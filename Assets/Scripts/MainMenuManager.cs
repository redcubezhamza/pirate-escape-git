using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenuManager : MonoBehaviour
{
    [Space(5)]
    [Header("Main Menu Buttons")]
    [SerializeField] GameObject settingsPanel;
    [SerializeField] GameObject shopPanel;
    [SerializeField] TextMeshProUGUI gemsText;


    [SerializeField] float closeTweenDelay = 0.5f, tweenDuration = 1;
    [SerializeField] LeanTweenType easeTween;

    // Start is called before the first frame update
    void Start()
    {
        // Screen.orientation = ScreenOrientation.Portrait;


        UpdateGems();
        
        
    }

    
    public void ShowSettingsPanel()
    {
        settingsPanel.transform.localScale = Vector3.zero;
        settingsPanel.gameObject.SetActive(true);
        settingsPanel.LeanScale(new Vector3(1f, 1f, 1f), tweenDuration).setEase(easeTween);

       // LeanTween.move(volumePanel.GetComponent<RectTransform>(), new Vector3(200f, -100f, 0f), 1f).setDelay(1f);
    }

    public void CloseSettingsPanel()
    {
        settingsPanel.LeanScale(Vector3.zero, tweenDuration / 3).setEaseInExpo().setOnComplete(AfterCloseAnim);
    }

    void AfterCloseAnim()
    {
        settingsPanel.gameObject.SetActive(false);
    }


    public void ShowClickedPanel(GameObject panelToOpen)
    {
        panelToOpen.gameObject.SetActive(true);
        panelToOpen.LeanScale(new Vector3(1.2f, 1.2f, 1), tweenDuration).setEase(easeTween);

        // LeanTween.move(volumePanel.GetComponent<RectTransform>(), new Vector3(200f, -100f, 0f), 1f).setDelay(1f);
    }

    public void CloseClickedPanel(GameObject panelToClose)
    {
        
        panelToClose.LeanScale(new Vector3(0.8f, 0.8f, 1), tweenDuration).setEaseInExpo();

    }


    public void AfterCloseTween(GameObject closingPanel)
    {
        StartCoroutine(PanelCloseDelay(closingPanel));
    }



    IEnumerator PanelCloseDelay(GameObject ClosedPanel)
    {
        yield return new WaitForSecondsRealtime(closeTweenDelay);
        ClosedPanel.gameObject.SetActive(false);

    }



    public void PlayButton()
    {
        if (!PlayerPrefs.HasKey("sceneindex"))
        {
            PlayerPrefs.SetInt("sceneindex", 1);

            SceneManager.LoadScene(1);
            
        }
        else
        {
            SceneManager.LoadScene( PlayerPrefs.GetInt("sceneindex"));
        }
    }


    public void UpdateGems()
    {
        gemsText.text = PlayerPrefs.GetInt("playergems", 0).ToString();
    }


    



}
