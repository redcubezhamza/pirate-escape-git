using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCameraSImple : MonoBehaviour
{

    [SerializeField] Transform target;

    [SerializeField] Vector3 offsetVector = new Vector3(5, 5, 5);

    private Vector3 lastOffset = new Vector3(0f, 0f, -1f);


    [SerializeField] float xOffset = 0, heightOffset = 10, distanceOffset = 15f, posLerpSpeed = 5f;
    // Start is called before the first frame update
    void Start()
    {
        //With your other variables

    }

    // Update is called once per frame
    void Update()
    {
        //Then inside your update function
        Vector3 offset = new Vector3(xOffset, heightOffset, -distanceOffset);
        Vector3 wantedCameraPosition = target.rotation * offset;

        Vector3 smoothedPos = Vector3.Lerp(lastOffset, wantedCameraPosition, Time.deltaTime * posLerpSpeed);
        transform.position = smoothedPos + target.position;
        //Remember to keep track of the last offset
        lastOffset = smoothedPos;
    }

}   

 
    

