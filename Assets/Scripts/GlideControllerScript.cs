using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlideControllerScript : MonoBehaviour
{
    

    public float steerAmount = 0f;
    public float steerAngle;
    public float forwardAccel;

    [SerializeField] Rigidbody rb;
    [SerializeField] float dirX;
    [SerializeField] float moveSpeed = 10f;
    [SerializeField] float xBoundary = 30;

    [SerializeField] bool touchControl;
    [SerializeField] float horizontalInput, verticalInput;

    [SerializeField] SpawnManager spawnManager;
    [SerializeField] GameObject destroyEffect;

    [SerializeField] float movementThresold = 10;
    [SerializeField] float steerFactor = 1;


    private EnemyAgent police;
    private Vector3 eulerRotation;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        police = GameObject.Find("Police").GetComponent<EnemyAgent>();

        eulerRotation = transform.rotation.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        
            dirX = Input.acceleration.x * moveSpeed;
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, -xBoundary, xBoundary), transform.position.y, transform.position.z);


        //steerAngle = Mathf.Clamp(steerAngle, -20 , 20);
             //rb.velocity = new Vector3(Mathf.Clamp(rb.velocity.x, -2, 2), rb.velocity.y, rb.velocity.z);
        
        


        





    }

    private void FixedUpdate()
    {
        Steering();
        MoveForward();

        //if (Application.platform == RuntimePlatform.Android)
        //{
        //    rb.velocity = new Vector3(dirX, 0f, 10f);
        //}

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            horizontalInput = Input.GetAxis("Horizontal");
            verticalInput = Input.GetAxis("Vertical");

            transform.Translate(horizontalInput * moveSpeed * Time.deltaTime, 0, verticalInput * moveSpeed * Time.deltaTime);
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("spawnnextsea"))
        {
            spawnManager.MoveSea();
            spawnManager.SpawnObstacles();
           // spawnManager.SpawnCoins();
        }

        //if (other.gameObject.CompareTag("coin"))
        //{
        //    HUDManager.instance.UpdateCoinScore(other.gameObject.GetComponent<Coin>().coinScores);

        //    Destroy(other.gameObject);
        //}
    }


    public void Steering()
    {
        //steer = Mathf.MoveTowards(steer, steerAmount, 0.5f);
        // rb.velocity = new Vector3(turnMultiplier, myRigidbody.velocity.y, myRigidbody.velocity.z);

        transform.localPosition += new Vector3(Mathf.MoveTowards(0, steerAmount, 0.3f), 0, 0);


        if (steerAngle == 0)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler( eulerRotation.x, 0, eulerRotation.z) , 2f * Time.deltaTime);
            
        }
        else
        {
            
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(eulerRotation.x, steerAngle, steerAngle / 6), 2f * Time.deltaTime);
            //  transform.rotation = Quaternion.Euler(eulerRotation.x, steerAngle, eulerRotation.z);
        }




        //  transform.Rotate(Vector3.up, steerAngle * Time.deltaTime);



    }

    public void MoveForward()
    {
       // rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, 10/*forwardAccel*/);

        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);

        
        


    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("triggerObstacle"))
        {
            StartCoroutine(police.GoNearPlayer());
        }


        if (collision.gameObject.CompareTag("stone"))
        {
            //destroyEffect.gameObject.SetActive(true);
            StartCoroutine(police.GoNearPlayer());

        }



        if (collision.gameObject.CompareTag("obstacleCluster"))
        {
            //float bounce = 10000f; //amount of force to apply
            //rb.AddForce(collision.contacts[0].normal * bounce);


          //  rb.AddForce(-Vector3.forward * 10);
           // isBouncing = true;
        }
        
    }


}
