using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using TMPro;

public class BuyShopItem : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI itemName;
    [SerializeField] int itemPrice;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BuyItem()
    {
        var request = new SubtractUserVirtualCurrencyRequest
        {
            VirtualCurrency = "GM",
            Amount = itemPrice

        };

        PlayFabClientAPI.SubtractUserVirtualCurrency(request, OnSubtractGemSuccess, OnError);
    }


    void OnSubtractGemSuccess(ModifyUserVirtualCurrencyResult result)
    {
        Debug.Log("Item Bought " + itemName);
        PlayfabManager.instance.GetVirtualCurrencies();
    }

    void OnError(PlayFabError error)
    {
        Debug.Log("Error while logging In/Creating account");
        Debug.Log(error.GenerateErrorReport());
    }
}
