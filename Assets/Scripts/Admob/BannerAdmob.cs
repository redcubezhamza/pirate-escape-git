using UnityEngine;
using GoogleMobileAds.Api;


public class BannerAdmob : MonoBehaviour
{
    public BannerView bannerAd;
    [SerializeField] string bannerAdID = "ca-app-pub-3940256099942544/6300978111";

    public static BannerAdmob Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        MobileAds.Initialize(InitializationStatus => { });

        this.RequestBanner();
    }

    public AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder().Build();
    }

    public void RequestBanner()
    {
        string adUnitId = bannerAdID;
        this.bannerAd = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        this.bannerAd.LoadAd(this.CreateAdRequest());
    }

    public void RequestCustomBanner()
    {
        string adUnitId = bannerAdID;
        AdSize adSize = new AdSize(600, 600);
        BannerView bannerView = new BannerView(adUnitId, adSize, AdPosition.Bottom);

        this.bannerAd = bannerView;
        this.bannerAd.LoadAd(this.CreateAdRequest());
    }


    public void DestroyBanner()
    {
        this.bannerAd.Destroy();
    }
}

