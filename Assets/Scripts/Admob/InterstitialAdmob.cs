using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class InterstitialAdmob : MonoBehaviour
{
    public InterstitialAd interstitial;

    [SerializeField] string interstitialAdID = "ca-app-pub-3940256099942544/1033173712";

    public static InterstitialAdmob Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        MobileAds.Initialize(InitializationStatus => { });
    }

    public AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder().Build();
    }

    public void RequestInterstitial()
    {
        string adUnitId = interstitialAdID;
        if (this.interstitial != null)
            this.interstitial.Destroy();

        this.interstitial = new InterstitialAd(adUnitId);
        this.interstitial.LoadAd(this.CreateAdRequest());
    }

    public void ShowInterstitial()
    {
        if (this.interstitial.IsLoaded())
        {
            interstitial.Show();
        }
        else
        {
            Debug.Log("Interstitial is not ready yet");
            RequestInterstitial();
        }
    }





















    //public void RequestRewardBasedVideo()
    //{
    //    string adUnitId = "ca-app-pub-3940256099942544/5224354917";

    //    //this.rewardBasedVideo.LoadAd(this.CreateAdRequest(), adUnitId);
    //    this.rewardBasedVideo = new RewardedAd(adUnitId);
    //    this.rewardBasedVideo.LoadAd(this.CreateAdRequest());
    //}

    //public void ShowRewardBasedVideo()
    //{
    //    if (this.rewardBasedVideo.IsLoaded())
    //    {
    //        this.rewardBasedVideo.Show();
    //    }
    //}

    //#region RewardBasedVideo callback handlers

    //public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    //{
    //    this.RequestRewardBasedVideo();
    //}

    //public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    //{
    //    isRewarded = true;
    //}

    //#endregion
}
