using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitAds : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        BannerAdmob.Instance.RequestBanner();

        InterstitialAdmob.Instance.RequestInterstitial();
        RewardedAdmob.Instance.RequestRewarded();
    }

    
}
