using UnityEngine;
using GoogleMobileAds.Api;
using System;
using UnityEngine.SceneManagement;

public class RewardedAdmob : MonoBehaviour
{
    public RewardedAd rewarded;
    [SerializeField] string rewardedAdID = "ca-app-pub-3940256099942544/5224354917";
    [SerializeField] int gemsToReward = 1;


    [SerializeField] Scene currentScene;
    [SerializeField] string currentSceneName;
    


   
    public static RewardedAdmob Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        MobileAds.Initialize(InitializationStatus => { });

        currentScene = SceneManager.GetActiveScene();
        currentSceneName= currentScene.name;
        
    }

    public AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder().Build();
    }

    public void RequestRewarded()
    {
        string adUnitId = rewardedAdID;
        if (this.rewarded != null)
            this.rewarded.Destroy();

        this.rewarded = new RewardedAd(adUnitId);
        this.rewarded.LoadAd(this.CreateAdRequest());
    }

    public void ShowRewarded()
    {
        
        if (this.rewarded.IsLoaded())
        {
            rewarded.Show();

        }
        else
        {
            Debug.Log("Rewarded is not ready yet");
            RequestRewarded();
        }


        this.rewarded.OnAdClosed += HandleRewardedAdClosed;
    }

    void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        this.RequestRewarded();
        Debug.Log("Loading Next Rewarded Ad.....");


        int gem = PlayerPrefs.GetInt("playergems");
        gem += 1;
        PlayerPrefs.SetInt("playergems", gem);


        if (currentSceneName == "MainMenu")
        {
            GameObject.Find("_scripts").GetComponent<MainMenuManager>().UpdateGems();
        }
         
    }

    



}

