using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip launchSound;
    [SerializeField] List<AudioClip> explosionSounds;
    private GameObject playerBoat;
    private int angle = 0;
    private Rigidbody rb;

    [SerializeField] float zAngle = 10;
    [SerializeField] GameObject explosionparticle;
    [SerializeField] GameObject cannonChildObject;

    [SerializeField] float lifeAfterImpact = 0.5f;
    public float radius = 5.0F;
    public float power = 10.0F;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        playerBoat = GameObject.FindGameObjectWithTag("boat");
        

        rb = GetComponent<Rigidbody>();
        audioSource.PlayOneShot(launchSound);


        //explosion force

        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
                rb.AddExplosionForce(power, explosionPos, radius, 3.0F);
        }


    }

    // Update is called once per frame
    void Update()
    {
        //angle += 1;
        //transform.Rotate(Vector3.right, angle * Time.deltaTime);

        transform.Rotate(0, 0, zAngle, Space.Self);

       // transform.LookAt(playerBoat.transform.position);

    }



    private void OnCollisionEnter(Collision collision)
    {
        //if (collision.gameObject.CompareTag("sea"))
        //{
        //    explosionparticle.gameObject.SetActive(true);
        //    Destroy(gameObject, 1);
        //}


        if (!collision.gameObject.CompareTag("boat"))
        {

            Instantiate(explosionparticle, new Vector3(transform.position.x, transform.position.y + 2, transform.position.z), Quaternion.identity);
            cannonChildObject.gameObject.SetActive(false);

            //select explosion sound to play
            if (explosionSounds[0] != null)
            {
                int index = Random.Range(0, explosionSounds.Count);
                audioSource.PlayOneShot(explosionSounds[index]);
                
            }
            Destroy(gameObject, lifeAfterImpact);


        }
        else
        {
            GameManager.instance.GameOver();
        }
    }
}
