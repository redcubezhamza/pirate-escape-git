using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleImages : MonoBehaviour
{
    [SerializeField] Sprite toggleOnSprite, toggleOffSprite;
    [SerializeField] Image imageToToggle;
    [SerializeField] bool On;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ToggleImage()
    {
        if (On == true)
        {
            On = false;
            imageToToggle.sprite = toggleOffSprite;
        }
        else 
        {
            On = true;
            imageToToggle.sprite = toggleOnSprite;
        }

        
    }
}
