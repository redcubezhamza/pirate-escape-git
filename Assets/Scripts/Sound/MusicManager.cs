using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;

    [SerializeField] AudioSource musicSource, sirenSource, sfxSource, bgMusicSource;

    [Header("SFX Sounds")]
    [SerializeField] AudioClip popUpSound;
    [SerializeField] AudioClip closeSound; 
    [SerializeField] AudioClip selectSound;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }


    public void PlayOneTimeSound(AudioClip effectToPlay)
    {
        sfxSource.PlayOneShot(effectToPlay);
    }

    public void PlayBGMusic(AudioClip musicToPlay)
    {
        musicSource.clip = musicToPlay;
        musicSource.Play();
    }

    public void PlaySirenSoundOnRepeat(AudioClip sirenSound)
    {
        sirenSource.clip = sirenSound;
        sirenSource.Play();
        
    }

    public void ChangeMasterVolume(float value)
    {
        AudioListener.volume = value;
    }


    public void ToggleMusic()
    {
        musicSource.mute = !musicSource.mute;
        bgMusicSource.mute = !bgMusicSource.mute;
        sirenSource.mute = !sirenSource;
    }

    public void ToggleSfx()
    {
        sfxSource.mute = !sfxSource.mute;
    }


    public void PlayCloseSFX()
    {
        PlayOneTimeSound(closeSound);
    }

    public void PlaySelectSFX()
    {
        PlayOneTimeSound(selectSound);
    }

    public void PlayPopupSFX()
    {
        PlayOneTimeSound(popUpSound);
    }
}
