using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleSounds : MonoBehaviour
{
    [SerializeField] bool toggleMusic, toggleSfx;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Toggle()
    {
        if (toggleMusic) MusicManager.instance.ToggleMusic();
        if (toggleSfx) MusicManager.instance.ToggleSfx();
    }

    
}
