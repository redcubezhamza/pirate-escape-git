using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MasterVolume : MonoBehaviour
{
    [SerializeField] Slider masterVolumeSlider;
    // Start is called before the first frame update
    void Start()
    {
       // MusicManager.instance.ChangeMasterVolume(masterVolumeSlider.value);
        masterVolumeSlider.onValueChanged.AddListener(val => MusicManager.instance.ChangeMasterVolume(val));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
