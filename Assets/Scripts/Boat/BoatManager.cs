using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatManager : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip coinCollectSound, shipCollisionClip;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coin"))
        {
            HUDManager.instance.UpdateCoinScore(other.gameObject.GetComponent<Coin>().coinScores);
            MusicManager.instance.PlayOneTimeSound(coinCollectSound);
          //  audioSource.PlayOneShot(coinCollectSound);
            Destroy(other.gameObject);
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("stone"))
        {
           this.gameObject.GetComponent<TimeBar>().SetValue(collision.gameObject.GetComponent<Stone>().Damage);
            audioSource.PlayOneShot(shipCollisionClip);
        }
    }
}
