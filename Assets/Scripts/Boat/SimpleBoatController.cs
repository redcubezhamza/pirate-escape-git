using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBoatController : MonoBehaviour
{
    [SerializeField] float forwardSpeed = 10, turnSpeed = 5;

    [SerializeField] float horizontalInput;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        horizontalInput = Input.GetAxis("Horizontal");


        transform.Translate(Vector3.forward * Time.deltaTime * 10);

        transform.Translate(Vector3.right * horizontalInput * Time.deltaTime * turnSpeed);
    }
}
