using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public int coinScores = 5;

    [SerializeField] float rotationSpeed = 10.5f;
    // Start is called before the first frame update
    void Start()
    {
        //Destroy(gameObject, Random.Range(20, 40));
    }

    private void Update()
    {
        transform.Rotate(Vector3.right * rotationSpeed * Time.deltaTime);
    }



    // Update is called once per frame





    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("boat"))
    //    {
    //        HUDManager.instance.UpdateCoinScore(coinScores);

    //        Destroy(gameObject);
    //    }

    //}
}
