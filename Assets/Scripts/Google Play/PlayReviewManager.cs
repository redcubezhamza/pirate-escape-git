using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Google.Play.Review;
public class PlayReviewManager : MonoBehaviour
{

    // Create instance of ReviewManager
    private ReviewManager _reviewManager;
    
    private PlayReviewInfo _playReviewInfo;

   // _reviewManager = new ReviewManager();

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(RequestReviewInfo());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator RequestReviewInfo()
    {
        var requestFlowOperation = _reviewManager.RequestReviewFlow();
        yield return requestFlowOperation;
        if (requestFlowOperation.Error != ReviewErrorCode.NoError)
        {
            // Log error. For example, using requestFlowOperation.Error.ToString().
            yield break;
        }
        _playReviewInfo = requestFlowOperation.GetResult();



        StartCoroutine(LaunchReviewFlow());
    }


    IEnumerator LaunchReviewFlow()
    {
        var launchFlowOperation = _reviewManager.LaunchReviewFlow(_playReviewInfo);
        yield return launchFlowOperation;
        _playReviewInfo = null; // Reset the object
        if (launchFlowOperation.Error != ReviewErrorCode.NoError)
        {
            Debug.Log("Play Review Error:red;");

            // Log error. For example, using requestFlowOperation.Error.ToString().
            yield break;
        }
        else
            Debug.Log("Play Review NOOOOO Error:green;");

        
        // The flow has finished. The API does not indicate whether the user
        // reviewed or not, or even whether the review dialog was shown. Thus, no
        // matter the result, we continue our app flow.
    }


}
