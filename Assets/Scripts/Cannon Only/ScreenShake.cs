using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    // Start is called before the first frame update
    public static float ShakeAmount = 0;
    Vector3 StartingPosition;

    void Start()
    {
        StartingPosition = transform.position;
    }

    void Update()
    {
        if (ShakeAmount != 0)
        {
            ShakeAmount = Mathf.Lerp(ShakeAmount, 0, 0.01f);
            transform.position = transform.position + Random.onUnitSphere * ShakeAmount;
        }
        
       
    }
}
