using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAgent : MonoBehaviour
{
   // private NavMeshAgent enemy;
    public Transform Player;

    [SerializeField] Transform missileLaunchPos;
    [SerializeField] Vector3 offset;
    [SerializeField] GameObject missilePrefab;
  //  [SerializeField] float missileLaunchVelocity = 500;
    [SerializeField] float missileSpeed = 20;
    [SerializeField] float minMissileSpeed = 30, maxMissileSpeed = 55;

    [SerializeField] float moveSpeed = 5;
    [SerializeField] AudioClip sirenSound;

    public bool nearToPlayer;
   
    // Start is called before the first frame update
    void Start()
    {
        // enemy = GetComponent<NavMeshAgent>();

        StartCoroutine(LaunchMissiles());

        MusicManager.instance.PlaySirenSoundOnRepeat(sirenSound);
    }

    // Update is called once per frame
    void Update()
    {
       transform.position = (Player.transform.position - offset);

       // transform.position = Vector3.MoveTowards(transform.position, Player.transform.position - offset, moveSpeed * Time.deltaTime);
        //enemy.SetDestination(Player.position);


        if (nearToPlayer == true)
        {
            offset.z = 30;
        }
        else
            offset.z = 50;



        if (Input.GetKeyDown(KeyCode.Space))
        {
            LaunchMissile();
        }
    }


    IEnumerator LaunchMissiles()
    {
        yield return new WaitForSeconds(Random.Range(4, 20));
        LaunchMissile();
        yield return new WaitForSeconds(Random.Range(4, 20));
        LaunchMissile();

        StartCoroutine(LaunchMissiles());


    }

    public IEnumerator GoNearPlayer()
    {
        if (!nearToPlayer)
        {
            nearToPlayer = true;
            ScreenShake.ShakeAmount = 0.5f;
            yield return new WaitForSeconds(1);
            ScreenShake.ShakeAmount = 0;
            yield return new WaitForSeconds(9);
            nearToPlayer = false;
        }
        else
            GameManager.instance.GameOver();
        
    }


    void LaunchMissile()
    {

        missileLaunchPos.transform.Rotate(Vector3.up, Random.Range(-10, 10));
        missileSpeed = Random.Range(minMissileSpeed, maxMissileSpeed);

        GameObject missile = Instantiate(missilePrefab, missileLaunchPos.position, missileLaunchPos.rotation  /*Quaternion.AngleAxis(Random.Range(-25, 25), new Vector3(0, 1, 0)*/);

        missile.GetComponent<Rigidbody>().velocity = missileLaunchPos.forward * missileSpeed;


        //GameObject proj = Instantiate(missilePrefab) as GameObject;
        //var body = proj.GetComponent<Rigidbody>();
        //body.AddForce(Player.position - transform.position, ForceMode.Impulse);



    }
}
