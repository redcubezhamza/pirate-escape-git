using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Float : MonoBehaviour
{
    [SerializeField]
    float buoyancy_force, forceApplyPos_Y = 0.5f;
    Rigidbody rig;
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float y_pos = transform.position.y;
        if (y_pos <= forceApplyPos_Y)
        {
            rig.AddForce(transform.up * buoyancy_force, ForceMode.Force);
        }
    }
}
