using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatingPlayStore : MonoBehaviour
{
    [SerializeField] string androidBundleName;
    [SerializeField] string MoreGamesBundleID;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void RateUs()
    {
#if UNITY_ANDROID
        Application.OpenURL("market://details?id=" + androidBundleName);

#endif
    }

    public void MoreGames()
    {
#if UNITY_ANDROID
        Application.OpenURL("market://details?id=" + MoreGamesBundleID);

#endif
    }

}

